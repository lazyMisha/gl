import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import project.gl.Config;
import project.gl.logger.Log;

public class WebUiTest extends Config {

    @BeforeMethod
    public void before() {
        setUp();
    }

    @Test
    public void test_01_createTweet() {
        Log.step("1. Open page");
        open("https://twitter.com");

        Log.step("2. Login");
        web.homePage.clickLogin();
        web.loginPage.login();
        web.profilePage.checkLoggedIn();

        Log.step("3. Create tweet");
        String tweetStat = web.profilePage.getTweetsStat();
        String tweet = web.profilePage.createTweet();

        Log.step("4. Check created tweet");
        open("https://twitter.com", false);
        web.profilePage.checkCountOfTweetsIncreased(tweetStat);
        web.profilePage.checkTweetIsCreated(tweet);
    }

    @Test
    public void test_02_removeAllTweets() {
        Log.step("1. Open page");
        open("https://twitter.com");

        Log.step("2. Login");
        web.homePage.clickLogin();
        web.loginPage.login();
        web.profilePage.checkLoggedIn();

        Log.step("3. Create tweet");
        String tweetStat = web.profilePage.getTweetsStat();
        String tweet = web.profilePage.createTweet();

        Log.step("4. Check created tweet");
        open("https://twitter.com", false);
        web.profilePage.checkCountOfTweetsIncreased(tweetStat);
        web.profilePage.checkTweetIsCreated(tweet);

        Log.step("5. Remove last tweet");
        web.profilePage.removeLastTweet();
    }

    @Test
    public void test_03_signUpNegative() {
        Log.step("1. Open page");
        open("https://twitter.com");

        Log.step("2. Try to login with invalid data");
        web.homePage.clickLogin();
        web.loginPage.checkErrorMessage();
    }
}
