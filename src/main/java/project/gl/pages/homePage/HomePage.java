package project.gl.pages.homePage;

import project.gl.common.CommonPage;

public class HomePage extends CommonPage {

    public void clickLogin() {
        element("//*[contains(@class,'StaticLoggedOutHomePage-buttonLogin')]").click();
    }
}
