package project.gl.pages;

import project.gl.pages.profilePage.ProfilePage;
import project.gl.pages.loginPage.LoginPage;
import project.gl.pages.homePage.HomePage;

public class PagesContainer {

    public HomePage homePage = new HomePage();
    public LoginPage loginPage = new LoginPage();
    public ProfilePage profilePage = new ProfilePage();
}
