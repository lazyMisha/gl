package project.gl.pages.profilePage;

import project.gl.common.CommonPage;
import project.gl.utils.Check;
import project.gl.utils.TestData;

import static org.hamcrest.Matchers.is;

public class ProfilePage extends CommonPage {

    public void checkLoggedIn() {
        Check.that("Login name is displayed", element("//*[@class='DashboardProfileCard-userFields account-group']").isDisplayed());
    }

    public String createTweet() {
        String tweetText = "Hello from " + TestData.getPokemon() + " to " + TestData.getHogwarts();
        element("//*[@id='global-new-tweet-button']").click();
        element("//*[@class='tweet-box rich-editor is-showPlaceholder']").set(tweetText);
        element("//*[@id='Tweetstorm-tweet-box-0']//*[@class='SendTweetsButton EdgeButton EdgeButton--primary EdgeButton--medium js-send-tweets']//span[contains(text(),'Tweet')]").click();
        element("//*[@class='new-tweets-bar js-new-tweets-bar']").click();
        element("//body").click();
        refreshPage();
        return tweetText;
    }

    public void checkTweetIsCreated(String tweetText) {
        String actualTweet = element("//*[@class='content']//p").getText();
        Check.that("Tweet is displayed", actualTweet, is(tweetText));
    }

    public void checkCountOfTweetsIncreased(String oldCount) {
        String actualCountOfTweets = getTweetsStat();
        Check.that("Count of tweets increased", Integer.parseInt(oldCount.replaceAll("[^0-9]", ""))
                < Integer.parseInt(actualCountOfTweets.replaceAll("[^0-9]", "")));
    }

    public String getTweetsStat() {
        return element("//*[@class='ProfileCardStats-statValue']").getText();
    }

    public void removeLastTweet() {
        String tweetToRemove = element("//*[@class='content']//p").getText();
        element("//*[@class='dropdown']//*[@class='ProfileTweet-actionButton u-textUserColorHover dropdown-toggle js-dropdown-toggle']")
                .getWebElements()
                .get(0)
                .click();
        executeJS("//*[contains(@class,'dropdown-menu')]//*[@role='menu']//*[@class='dropdown-link' and contains(text(),'Delete Tweet')]", "arguments[0].click();");
        executeJS("//*[@class='EdgeButton EdgeButton--danger delete-action']", "arguments[0].click();");
        refreshPage();
        String lastTweet = element("//*[@class='content']//p").getText();
        Check.that("Tweet is removed", !tweetToRemove.equals(lastTweet));
    }
}
