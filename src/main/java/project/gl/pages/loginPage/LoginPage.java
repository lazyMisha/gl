package project.gl.pages.loginPage;

import project.gl.common.CommonPage;
import project.gl.utils.Check;
import project.gl.utils.TestData;

public class LoginPage extends CommonPage {

    public void login() {
        element("//*[contains(@class,'username-field email-input')]").set("Lazy28857074");
        element("//*[@class='js-password-field']").set("Pass1234567890ForTest");
        element("//button[contains(@class,'submit')]").click();
    }

    public void checkErrorMessage() {
        element("//*[contains(@class,'username-field email-input')]").set(TestData.getName());
        element("//*[@class='js-password-field']").set(TestData.getPhone());
        element("//button[contains(@class,'submit')]").click();
        Check.that("Error message is displayed", element("//*[@class='message-text']").isDisplayed());
    }
}
