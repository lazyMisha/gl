package project.gl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import project.gl.logger.Log;
import project.gl.pages.PagesContainer;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Config {

    private static WebDriver webDriver;
    private static String workingDir;
    private static String slash;
    private static String OS;

    protected PagesContainer web;

    static {
        workingDir = System.getProperty("user.dir");
        slash = File.separator;
        OS = System.getProperty("os.name").replaceAll("[^a-zA-Z]", "");
        System.setProperty("webdriver.chrome.driver", workingDir + slash + "src" + slash + "main" + slash + "resources" + slash + "chromedriver_" + OS + (OS.equals("Linux") ? "" : ".exe"));
    }

    protected void setUp() {
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
        webDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        web = new PagesContainer();
    }

    protected WebDriver getWebDriver() {
        return webDriver;
    }

    public void open(String URL) {
        open(URL, true);
    }

    public void open(String URL, boolean clearCookie){
        if (clearCookie) {
            webDriver.manage().deleteAllCookies();
            Log.info("Clear cookies");
        }
        webDriver.get(URL);
        Log.info("Open '" + URL + "'");
    }

    public void refreshPage() {
        String currentUrl = getLocation();
        Log.info("Refresh page " + currentUrl);
        webDriver.navigate().refresh();
    }

    public String getLocation() {
        Log.info("Try to get current URL");
        return webDriver.getCurrentUrl();
    }

    @AfterMethod
    public void tearDown() {
        if (webDriver != null) {
            webDriver.quit();
        }
        Log.info("Finish!");
    }
}
