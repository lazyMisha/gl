package project.gl.common;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import project.gl.Config;
import project.gl.logger.Log;

public class CommonPage extends Config {

    protected Element element(String xpath) {
        return new Element(xpath, getWebDriver());
    }

    protected void executeJS(String xpath, String JS) {
        Log.info("Try to execute JS " + JS);
        ((JavascriptExecutor) getWebDriver()).executeScript(JS, getWebDriver().findElement(By.xpath(xpath)));
    }
}
