package project.gl.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import project.gl.logger.Log;

import java.util.Arrays;
import java.util.List;

public class Element {

    private String xpath;
    private WebDriver webDriver;
    private WebElement element;
    private Actions actions;
    private WebDriverWait wait;

    Element(String xpath, WebDriver webDriver) {
        this.xpath = xpath;
        this.webDriver = webDriver;
        actions = new Actions(webDriver);
        wait = new  WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void click() {
        Log.info("Try to click the: '" + xpath + "'");
        actions.moveToElement(element).click().perform();
    }

    public void set(CharSequence... keys) {
        actions.moveToElement(element);
        String key = Arrays.toString(keys);
        Log.info("Try to set: '" + key + "' to the: '" + xpath + "'");
        element.sendKeys(keys);
    }

    public String getText() {
        actions.moveToElement(element);
        String text = element.getText();
        Log.info("Try to get text from: '" + xpath + "'" + "\n" + "text: '" + text + "'");
        return text;
    }

    public boolean isDisplayed() {
        actions.moveToElement(element);
        Log.info("Get visibility of '" + xpath + "'");
        return !webDriver.findElements(By.xpath(xpath)).isEmpty();
    }

    public List<WebElement> getWebElements() {
        Log.info("Try to get all WebElements by xpath: " + xpath);
        return webDriver.findElements(By.xpath(xpath));
    }
}
