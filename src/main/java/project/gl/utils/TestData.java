package project.gl.utils;

import com.github.javafaker.Faker;

public class TestData {

    private static Faker faker = new Faker();

    public static String getPokemon() {
        return faker.pokemon().name();
    }

    public static String getHogwarts() {
        return faker.harryPotter().character();
    }

    public static String getName() {
        return faker.artist().name();
    }

    public static String getPhone() {
        return faker.phoneNumber().phoneNumber();
    }
}