package project.gl.logger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Log extends Level {

    private static Logger Log = Logger.getLogger(Log.class.getName());
    private static final Level STEP = new Log(30000, "STEP", 5);
    private static final Level PASSED = new Log(30000, "PASSED", 5);

    private Log(int level, String levelStr, int syslogEquivalent) {
        super(level, levelStr, syslogEquivalent);
    }

    public static void step (String step) {
        Log.log(STEP, step);
    }

    public static void info (String message) {
        Log.info(message);
    }

    public static void warn (String message) {
        Log.warn(message);
    }

    public static void passed(String message) {
        Log.log(PASSED, message);
    }
}